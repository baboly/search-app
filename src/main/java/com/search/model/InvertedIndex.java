package com.search.model;

import java.util.List;
import java.util.Map;

/**
 * Created by Babak Tamjidi on 2017-02-06.
 */

public class InvertedIndex {

    private static InvertedIndex invertedIndexInstance = new InvertedIndex();
    private Map<String, Map<Double, List<Integer>>> invertedIndex;

    private InvertedIndex() {
    }

    public static synchronized InvertedIndex getInstance() {
        if (invertedIndexInstance == null) {
            invertedIndexInstance = new InvertedIndex();
        }
        return invertedIndexInstance;
    }

    public Map<String, Map<Double, List<Integer>>> getInvertedIndex() {
        return invertedIndex;
    }

    public void setInvertedIndex(Map<String, Map<Double, List<Integer>>> invertedIndex) {
        this.invertedIndex = invertedIndex;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("Clone is not Supported");
    }
}
