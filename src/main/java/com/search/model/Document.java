package com.search.model;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Babak Tamjidi on 2017-02-06.
 */
public class Document {

    private String text;

    public Document(String text){
        this.text = text;
    }

    private static List<Document> documents = Arrays.asList(
            new Document("the brown fox jumped over the brown dog"),
            new Document("the lazy brown dog sat in the corner and his head was brown and one leg was white"),
            new Document("the red fox bit the lazy dog"),
            new Document("the lazy brown dog sat in the corner and his flower was brown and blue"),
            new Document("the lazy brown dog sat in the corner and the flower was brown and yellow this was not ok")
    );

    public String getText() {
        return text;
    }

    public static List<Document> getDocuments() {
        return documents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Document document = (Document) o;
        return getText().equals(document.getText());
    }

    @Override
    public int hashCode() {
        return getText().hashCode();
    }
}
