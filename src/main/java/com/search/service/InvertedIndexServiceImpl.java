package com.search.service;

import com.search.model.Document;
import com.search.model.InvertedIndex;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

/**
 * Created by Babak Tamjidi on 2017-02-06.
 * <p>
 * {@code buildInvertedIndex} method gets a List of documents and loops each document.
 * Every word in the document get extracted with a regex and added to the String[] {@code words}.
 * Afterwards we stream String[] {@code words} and group the words and count how many times they occurs and save it to the Map {@code wordMap}.
 * Then it loops the Map {@code wordMap} and puts each word as a key and the value of the key is a TreeMap that is sorted with
 * those words that occur most in each document first and then descending. It saves the count of how many times the word occur as key and a List<Integer> as value. There it stores
 * the index of the documents that have that word the same amount that it occurs.
 */
public class InvertedIndexServiceImpl implements InvertedIndexService<Document> {

    @Override
    public void buildInvertedIndex(List<Document> documents) {
        Map<String, Map<Double, List<Integer>>> index = new HashMap<>();
        for (Document document : documents) {
            String[] words = document.getText().split("\\W+");
            Map<String, Long> wordMap = Arrays.stream(words)
                    .collect(groupingBy(name -> name.toLowerCase() , counting()));
            Map<String, Double> tfMap = new HashMap<>();
            for(String key : wordMap.keySet()){
                double value = wordMap.get(key);
                tfMap.put(key, value/words.length);
            }
            for (String word : tfMap.keySet()) {
                if (index.containsKey(word)) {
                    Map<Double, List<Integer>> wordOccurDocIndexMap = index.get(word);
                    double count = tfMap.get(word);
                    if (wordOccurDocIndexMap.get(count) != null && !wordOccurDocIndexMap.get(count).contains(documents.indexOf(document))) {
                        wordOccurDocIndexMap.get(count).add(documents.indexOf(document));
                    } else {
                        List<Integer> indexList = new ArrayList<Integer>() {{
                            add(documents.indexOf(document));
                        }};
                        wordOccurDocIndexMap.put(count, indexList);
                    }
                    index.put(word, wordOccurDocIndexMap);
                } else {
                    List<Integer> indexList = new ArrayList<Integer>() {{
                        add(documents.indexOf(document));
                    }};
                    Map<Double, List<Integer>> wordOccurDocIndexMap = new HashMap<>();
                    double count = tfMap.get(word);
                    wordOccurDocIndexMap.put(count, indexList);
                    index.put(word, wordOccurDocIndexMap);
                }
            }
        }
        Map<String, Double> idfMap = buildIdfMap(index, documents.size());
        Map<String, Map<Double, List<Integer>>> tfIdfIndex = buildTFIDFIndex(index, idfMap);

        InvertedIndex.getInstance().setInvertedIndex(tfIdfIndex);
    }

    @Override
    public Map<String, Double> buildIdfMap(Map<String, Map<Double, List<Integer>>> tfIndex, Integer docSize) {

        Map<String, Double> idfMap = new HashMap<>();
        for(String key : tfIndex.keySet()){
            Map<Double, List<Integer>> tfmap = tfIndex.get(key);
            double count = 0;
            for(List<Integer> list : tfmap.values()){
                count += list.size();
            }
            idfMap.put(key, Math.log10(docSize/count));
        }
        return idfMap;
    }

    @Override
    public Map<String, Map<Double, List<Integer>>> buildTFIDFIndex(Map<String, Map<Double, List<Integer>>> tfIndex, Map<String, Double> idfMap) {
        Map<String, Map<Double, List<Integer>>> tfIdfIndex = new HashMap<>();
        for(String key : tfIndex.keySet()){
            Map<Double, List<Integer>> tfIdfMap = new TreeMap<>(Collections.reverseOrder());
            Map<Double, List<Integer>> tfMap = tfIndex.get(key);
            double idf = idfMap.get(key);
            for(Double tf : tfMap.keySet()){
                BigDecimal bd = new BigDecimal(tf*idf);
                bd = bd.setScale(4, RoundingMode.DOWN);
                if(tfIdfMap.get(bd.doubleValue()) != null ){
                    tfIdfMap.get(bd.doubleValue()).addAll(tfMap.get(tf));

                }else {
                    tfIdfMap.put(bd.doubleValue(), tfMap.get(tf));
                }
            }
            tfIdfIndex.put(key, tfIdfMap);
        }
        return tfIdfIndex;
    }
}
