package com.search.service;

import java.util.List;
import java.util.Map;

/**
 * Created by Babak Tamjidi on 2017-02-06.
 */
public interface InvertedIndexService<T> {

    void buildInvertedIndex(List<T> list);

    Map<String, Double> buildIdfMap(Map<String, Map<Double, List<Integer>>> tfIndex, Integer docSize);

    Map<String, Map<Double, List<Integer>>> buildTFIDFIndex(Map<String, Map<Double, List<Integer>>> tfIndex, Map<String, Double> idfMap);


}
