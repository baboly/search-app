package com.search.controller;

import com.search.model.Document;
import com.search.model.InvertedIndex;
import com.search.service.InvertedIndexServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by Babak Tamjidi on 2017-02-06.
 */
@Controller
public class SearchController {

    private InvertedIndexServiceImpl invertedIndexServiceImpl;
    private InvertedIndex invertedIndex;
    private List<Document> documents;


    public SearchController() {
        invertedIndexServiceImpl = new InvertedIndexServiceImpl();
        invertedIndex = InvertedIndex.getInstance();
        documents = Document.getDocuments();
    }

    @GetMapping(value = "/")
    public String start() {
        if (invertedIndex.getInvertedIndex() == null) {
            invertedIndexServiceImpl.buildInvertedIndex(documents);
        }
        return "index";
    }

    @PostMapping(value = "/search")
    public String getResults(@RequestParam String query, Model model) {
        if(query.split("\\W+").length < 2){
            Map<Double, List<Integer>> integerListMap = invertedIndex.getInvertedIndex().get(query.toLowerCase().trim());
            if (integerListMap == null || integerListMap.size() < 1)
                model.addAttribute("message", "No result");
            else
                model.addAttribute("result", integerListMap);
        }else{
            model.addAttribute("message", "Only Singel Term Search Is Allowed In This Version");
        }
        model.addAttribute("documents", documents);
        return "index";
    }


}
