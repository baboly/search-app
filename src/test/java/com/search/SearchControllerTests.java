package com.search;

import com.search.controller.SearchController;
import com.search.model.Document;
import com.search.model.InvertedIndex;
import com.search.service.InvertedIndexServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SearchControllerTests {

	@Autowired
	private WebApplicationContext wac;

	@Mock
	private SearchController searchController;

	private MockMvc mockMvc;

	private InvertedIndexServiceImpl invertedIndexServiceImpl = new InvertedIndexServiceImpl();

	private List<Document> documents = Arrays.asList(
			new Document("the brown fox jumped over the brown dog"),
			new Document("the lazy brown dog sat in the corner and his head was brown and one leg was white"),
			new Document("the red fox bit the lazy dog"),
			new Document("the lazy brown dog sat in the corner and his flower was brown and blue"),
			new Document("the lazy brown dog sat in the corner and the flower was brown and yellow this was not ok")
	);

	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void verifiesHomePageLoads() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/"))
				.andExpect(status().isOk());
	}

	@Test
	public void verifiesDocumentList() throws Exception {
		List<Document> documents = Document.getDocuments();

		assertNotEquals(null, documents);
		assertNotEquals(0, documents.size());
		assertEquals(this.documents, documents);

		for(Document document : documents) {
			assertNotEquals(null, document);
		}
	}

	@Test
	public void verfiesSearchQuery() throws Exception {
		invertedIndexServiceImpl.buildInvertedIndex(documents);
		mockMvc.perform(post("/search")
				.param("query", "yellow"))
				.andExpect(status().isOk())
				.andExpect(model().attributeExists("result"))
				.andExpect(model().attributeExists("documents"))
				.andExpect(model().attribute("result", InvertedIndex.getInstance().getInvertedIndex().get("yellow")));

	}

}
